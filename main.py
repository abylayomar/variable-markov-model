import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from IPython.display import Audio
from numpy.fft import fft, ifft
%matplotlib inline

#READ AUDIO INPUT
Fs, data = read('bit-cr.wav')
datax = data[:,0]
data = []
silent = 70
for i in datax:
    if i >= silent or i <= -silent:
        data.append(i)      
print("Sampling Frequency is", Fs)

#CREATE AUDIO
Audio(data, rate=Fs)

#PLOT
plt.figure()
plt.plot(data)
plt.xlabel('Sample Index')
plt.ylabel('Amplitude')
plt.title('HipHop Beats')
plt.show()

#BUILD ORACLE
oracle = build_oracle(data[:100000], 'f') # We build only for first 10^5 indexes. Due to lack of time
Audio([data[i] for i in oracle.lrs], rate=Fs)

##PLOT ORACLE
plt.figure()
plt.plot(oracle.lrs)
plt.xlabel('Sample Index')
plt.ylabel('Amplitude')
plt.title('Oracle')
plt.show()