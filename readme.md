# Variable Marcov Oracle
This is the finall project of CSS503 Advanced Programming Technologies at [Suleiman Demirel University].
Lecturer [Birzhan Moldagaliyev]
Team Elise #15
### Description
This is the attempt of realization Variable Marcov Oracle [paper]. Important! the structure is not totlly realized.

### Required

* [Python] - Python 3.*
* [Pip] - PIP 19.*

### Installation

```sh
git clone git@gitlab.com:abylayomar/variable-markov-model.git
cd variable-markov-model
pip install -f requirements.txt
```

### Usage
There are main Variable Marcov Oracle jupyter file with all explanations.


   [Python]: <https://www.python.org/>
   [PIP]: <https://pypi.org/project/pip/>
   [Paper]: <http://musicgrad.ucsd.edu/~jhsu/documents/cwang_jhsu_sdubnov-vmo_theme_discovery.pdf>
   [Birzhan Moldagaliyev]: <https://sites.google.com/view/birzhanm/home>
   [Suleiman Demirel University]: <sdu.edu.kz>
 
